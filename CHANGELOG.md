# CHANGELOG

## [1.0.x (Unreleased)](https://gitlab.com/onlime/roundcube-plugins/jwt-sso/compare/1.0.2...master)


## [1.0.2 (2021-02-01)](https://gitlab.com/onlime/roundcube-plugins/jwt-sso/compare/1.0.1...1.0.2)

### Added
- Added `jwt_sso_ttl` config option to set length of time (in minutes) that the JWT token will be valid for (`exp` claim).
- Added CHANGELOG (the file you're looking at).

### Changed
- Changed JWT timestamp claims (`iat`, `nbf`, `exp`) precision from microseconds to seconds as this is better supported by other JWT libraries.

## [1.0.1 (2021-01-29)](https://gitlab.com/onlime/roundcube-plugins/jwt-sso/compare/1.0...1.0.1)

### Fixed
- Removed version from `composer.json` for automated Packagist submission

## 1.0 (2021-01-29)

- Initial release
